# Income_Quartile_Map_Gif

Income per census year has been divided into quartiles (2001, 2006, 2011, 2016) and then mapped onto  SA2s from across SE Qld. Data from the ABS census - historic median personal weekly income data extracted from AURIN. 